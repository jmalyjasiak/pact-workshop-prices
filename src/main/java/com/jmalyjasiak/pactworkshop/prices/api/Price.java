package com.jmalyjasiak.pactworkshop.prices.api;

import java.util.List;

public record Price(
        String productId,
        Money price,
        Unit unit
) {

    record Money(String currency, String amount) {}

    public enum Unit {
        EACH, PER_100G
    }

    public record Prices(List<Price> prices) {}
}
