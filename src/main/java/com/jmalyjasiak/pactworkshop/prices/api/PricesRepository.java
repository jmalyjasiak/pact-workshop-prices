package com.jmalyjasiak.pactworkshop.prices.api;

import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Component
public class PricesRepository {

    private final Map<String, Price> db = new ConcurrentHashMap<>();

    public List<Price> getAllPrices() {
        return List.copyOf(db.values());
    }

    public void savePrice(Price price) {
        db.put(price.productId(), price);
    }
}
