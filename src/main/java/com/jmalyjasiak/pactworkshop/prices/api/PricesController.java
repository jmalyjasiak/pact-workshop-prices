package com.jmalyjasiak.pactworkshop.prices.api;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class PricesController {

    private final PricesRepository pricesRepository;

    public PricesController(PricesRepository pricesRepository) {
        this.pricesRepository = pricesRepository;
    }

    @GetMapping("v1/prices")
    public Price.Prices findPrices() {
        return new Price.Prices(pricesRepository.getAllPrices());
    }

}