package com.jmalyjasiak.pactworkshop.prices;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.server.LocalServerPort;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

@SpringBootTest(webEnvironment = RANDOM_PORT)
class OpenApiGenerationTest {

    @LocalServerPort
    private int port;

    @Test
    void generateOpenApiArtifact() throws IOException, InterruptedException {
        var request = HttpRequest.newBuilder()
                .GET()
                .uri(URI.create("http://localhost:%s/v3/api-docs".formatted(port)))
                .build();

        var response = HttpClient.newHttpClient().send(request, HttpResponse.BodyHandlers.ofString());


        exportToFile("build/openapi.json", response.body());
    }

    private void exportToFile(String destination, String content) throws IOException {
        try (var writer = new BufferedWriter(new FileWriter(destination))) {
            writer.write(content);
        }
    }
}
